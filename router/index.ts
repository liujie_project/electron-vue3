import { createRouter, createWebHashHistory } from 'vue-router'

// 1. 定义路由组件.
// 也可以从其他文件导入
import home from "../src/pages/index.vue"
import purchaseOrder from "../src/pages/purchaseOrder/index.vue"
import salesSlip from "../src/pages/salesSlip/index.vue"
import warehouse from "../src/pages/warehouse/index.vue"


// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { path: '/', redirect: { name: "Home" } },
  { name: 'Home', path: '/home', component: home },
  { name: "PurchaseOrder", path: '/purchase-order', component: purchaseOrder },
  { name: "SalesSlip", path: '/sales-slip', component: salesSlip },
  { name: "Warehouse", path: '/warehouse', component: warehouse },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

// router.beforeEach((to, from) => {
//   // ...
//   // 返回 false 以取消导航
//   return false
// })

export default router;