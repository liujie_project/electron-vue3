import { app, BrowserWindow, Menu } from 'electron'
import path from 'path'
//app 控制应用程序的事件生命周期。
//BrowserWindow 创建并控制浏览器窗口。

let win: BrowserWindow | null;
//定义全局变量获取 窗口实例

const createWindow = () => {
  win = new BrowserWindow({
    show: false,
    webPreferences: {
      devTools: true,
      contextIsolation: false,
      nodeIntegration: true
    },
    // titleBarStyle: 'hidden',
    // frame: false
  })
  // 关闭 导航的菜单栏
  Menu.setApplicationMenu(null);

  // 打开调试模式
  win.webContents.openDevTools();

  win.on('ready-to-show', () => {
    win?.show();
  })

  if (process.env.NODE_ENV != 'development') {
    win.loadFile(path.join(__dirname, '..', 'dist/index.html'));
  } else {
    console.log(`${process.env['VITE_DEV_SERVER_URL']}#/home`);
    win.loadURL(`${process.env['VITE_DEV_SERVER_URL']}#/home`);
  }
}
// 在Electron完成初始化时被触发
app.whenReady().then(createWindow)



// if (win != null) {
//   win!.webContents.send('load', { message: "electron初始化了" });
// }

import "../server/index.ts"