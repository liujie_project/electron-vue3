import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import { useElementPlusUi } from './element-plus';
import router from "../router/index.ts";
import GoBack from "./components/goBack.vue";

const app = createApp(App);
app.use(router);
app.mount('#app');
useElementPlusUi(app);
app.component('GoBack',GoBack)