import type { App } from "vue";
import 'element-plus/dist/index.css'
import ElButton from 'element-plus';
import ElTable from 'element-plus';
import ElIcon from 'element-plus';
import ElDialog from 'element-plus';
import ElForm from 'element-plus';
import ElFormItem from 'element-plus';
import ElSelect from 'element-plus';
import ElOption from 'element-plus';
import ElInput from 'element-plus';

const components = [
  ElButton,
  ElTable,
  ElIcon,
  ElDialog,
  ElForm,
  ElFormItem,
  ElSelect,
  ElOption,
  ElInput
]

// 注册组件
export function useElementPlusUi(app: App) {
  components.forEach((e) => {
    app.use(e);
  })
}